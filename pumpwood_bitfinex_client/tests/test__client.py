import os
import pandas as pd
import unittest
from datetime import datetime
from pumpwood_bitfinex_client.client import BitfinexClient


cripto = BitfinexClient(
    api_key=os.environ.get('BITFINEX_API_KEY'),
    api_secret=os.environ.get('BITFINEX_API_SECRET'))


class TestRetrieveAccountInfo(unittest.TestCase):
    def test_get_wallet(self):
        cripto.get_wallet()

    def test_get_symbols_details(self):
        cripto.get_symbols_details()

    def test_list_pairs(self):
        cripto.list_pairs()

    def test_list_currencies(self):
        cripto.list_currencies()

    def test_get_currency_pair_candle(self):
        cripto.get_currency_pair_candle(
            base_currency="BTC",
            quote_currency="USD",
            start_time=datetime(2020, 1, 1),
            timeframe="30m")

    def test_get_ticker_info(self):
        response = cripto.get_ticker_info("tBTCUSD")
        self.assertEqual(len(response), 1)

        response = cripto.get_ticker_info(["tBTCUSD", "tETHUSD"])
        self.assertEqual(len(response), 2)

        cripto.get_ticker_info(tickers=["tBTCUSD", "XPOT"])

    def test_set_portfolio_share(self):
        portfolio_shares = pd.DataFrame([
            {"currency": "BTC", "share": 0.9},
            {"currency": "USD", "share": 0.1},
            {"currency": "LTC", "share": 0.0},
            {"currency": "ETH", "share": 0.0},
            {"currency": "XRP", "share": 0.0},
            {"currency": "BSV", "share": 0.0},
        ])
        results = cripto.set_portfolio_share(
            portfolio_shares=portfolio_shares)

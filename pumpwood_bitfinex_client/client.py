"""BitFinex API."""
import pandas as pd
import numpy as np
import requests
import simplejson as json
import time
import hashlib
import hmac
import math
from typing import List
from datetime import datetime
from .exceptions import BitFinexClientException


class BitfinexClient:
    """Client to get information from BitFinex and place orders."""

    PUB_API_URL = "https://api-pub.bitfinex.com/v2/"
    API_URL = "https://api.bitfinex.com/v2/"
    SIGNATURE_STR = '/api/{api_path}{nonce}{body}'

    def __init__(self, api_key: str = None, api_secret: str = None):
        """
        __init__.

        Args:
            No Args
        """
        self._api_key = api_key
        self._api_secret = api_secret
        self.pairs = self.list_pairs()
        self.currencies = self.list_currencies()

    def build_auth_header(self, end_point: str, body: dict) -> dict:
        """Create headers for authenticated calls on Bitfinex.

        Args:
            end_point [str]: Api end-point to be called.
            body [dict]: Body of the post message.
        Returns [dict]:
            A dict with the auth header at BitFinex.
        """
        nonce = str(time.time() * 100000)
        raw_body = json.dumps(body)
        signature = "/api/v2/{end_point}{nonce}{raw_body}".format(
            end_point=end_point, nonce=nonce, raw_body=raw_body)

        hmc = hmac.new(
            self._api_secret.encode('utf8'), signature.encode('utf8'),
            hashlib.sha384)
        signature = hmc.hexdigest()

        return {
            "bfx-nonce": nonce,
            "bfx-apikey": self._api_key,
            "bfx-signature": signature,
            "content-type": "application/json"}

    def post_authenticated(self, end_point: str, body: dict) -> any:
        """Post an authenticated call to Bitfinex.

        Args:
            end_point [str]: Api end-point to be called.
            body [dict]: Body of the post message.
        Return [any]:
            Serialized json of the authenticated post call.
        Exception:
            [BitFinexClientException] If there is any error when calling API,
                returns the text in the call with error.
        """
        if self._api_key is None or self._api_secret is None:
            raise BitFinexClientException(
                "Client not logged, can not perform authenticated API calls.")

        headers = self.build_auth_header(end_point, body)
        response = requests.post(
            self.API_URL + end_point, headers=headers,
            data=json.dumps(body))
        try:
            response.raise_for_status()
        except Exception:
            raise BitFinexClientException(response.text)
        return response.json()

    def get(self, end_point: str, parameters: dict = {}) -> any:
        """Make api calls at not authenticated api.

        Make api calls at not authenticated api and sleep for 30s if rate limit
        exceeded.

        Args:
            end_point [str]: Api end-point to be called.
            parameters [dict]: Parameters used in get url.
        Return [any]:
            Serialized json of the not-authenticated get call.
        """
        url = self.PUB_API_URL + end_point
        resp = requests.get(url, params=parameters)
        while resp.status_code != 200:
            if resp.status_code == 429:
                print("Rate limit reached, sleeping for 30''...")
                time.sleep(30)
                resp = requests.get(url, params=parameters)
            else:
                template = "Unknown error [{status_code}]: {msg}"
                raise Exception(template.format(
                    status_code=resp.status_code, msg=resp.text))
        return resp.json()

    def get_symbols_details(self) -> pd.DataFrame:
        """Get symbols details.

        Return [pd.DataFrame]:
            pair [str]: Currency pair.
            price_precision [float]: Price precision in trasacitons.
            initial_margin [float]: ?.
            minimum_margin [float]: ?.
            maximum_order_size [float]: Maximum amount that can be used in an
                order.
            minimum_order_size [float]: Minimum amount that can be used in an
                order.
            expiration [str]: ?.
            margin [bool]: If currency can be traded with margin.
        """
        url = "https://api.bitfinex.com/v1/symbols_details"
        pd_data = pd.DataFrame(
            requests.get(url).json(), columns=[
                "pair", "price_precision", "initial_margin",
                "minimum_margin", "maximum_order_size", "minimum_order_size",
                "expiration", "margin"])
        pd_data[[
            "initial_margin", "minimum_margin",
            "maximum_order_size", "minimum_order_size"]] = pd_data[[
                "initial_margin", "minimum_margin",
                "maximum_order_size", "minimum_order_size"]].astype(float)
        return pd_data

    def list_pairs(self) -> pd.DataFrame:
        """
        Return ticker info.

        Kwargs:
            symbol [str]: Set ticker to return info about

        Return [pd.DataFrame]:
            currency_pair [str]: Currency pair.
            min_order [float]: Minimum order amount.
            max_order [float]: Maximum order amount.
        """
        respose = self.get("conf/pub:info:pair")
        list_pair_info = []
        for pair in respose[0]:
            list_pair_info.append({
                "currency_pair": pair[0],
                "min_order": float(pair[1][3]),
                "max_order": float(pair[1][4]),
            })
        return pd.DataFrame(list_pair_info, columns=[
            "currency_pair", "min_order", "max_order"])

    def list_currencies(self) -> pd.DataFrame:
        """
        Return ticker info.

        Kwargs:
            symbol [str]: Set ticker to return info about

        Return:
            symbol: Bitfinex symbol.
            currency: Description of the currency.
        """
        response = self.get("conf/pub:map:currency:label")
        return pd.DataFrame(response[0], columns=[
            "symbol", "currency"])

    def get_currency_pair_candle(self, base_currency: str, quote_currency: str,
                                 start_time: datetime,
                                 order_type: str = "spot",
                                 timeframe: str = "30m"):
        """Make ticker candles from start date to end_time.

        Create ticker at bitfinex t{base_currency}{quote_currency} for spot
        values.

        Args:
            base_currency [str]: Base currency for pair.
            quote_currency [str]: Quote currency for pair.
        Kwargs:
            order_type [str] = 'spot': Only implemented for spot.
            order_type [str] = 'spot': Only implemented for spot.
            timeframe [str] = "30m": Timeframe to gather candle data, possible
                values [1m', '5m', '15m', '30m', '1h', '3h', '6h', '12h', '1D',
                '7D', '14D', '1M']
        """
        # Ajusting currency to upper case
        base_currency = base_currency.upper()
        quote_currency = quote_currency.upper()
        end_point = "candles/trade:{timeframe}:{ticker}/" + \
            "hist?limit=1000&start={start}&sort=1"

        if timeframe not in ['1m', '5m', '15m', '30m', '1h', '3h', '6h',
                             '12h', '1D', '7D', '14D', '1M']:
            msg = ("timeframe {} not available, possible values:\n"
                   "['1m', '5m', '15m', '30m', '1h', '3h', '6h', "
                   "'12h', '1D', '7D', '14D', '1M']").format(timeframe)
            raise BitFinexClientException(msg)
        if order_type != 'spot':
            raise BitFinexClientException("Only stop type is implemented")

        currency_pair = "{base_currency}{quote_currency}".format(
            base_currency=base_currency, quote_currency=quote_currency)
        if not (currency_pair == self.pairs["currency_pair"]).any():
            raise BitFinexClientException(
                "Currency pair is not available on Bitfinex.")

        ticker = None
        if order_type == "spot":
            ticker = "t" + currency_pair

        list_pd_data = []
        start_ms = start_time.timestamp() * 1000
        while True:
            template = "Geting candle data: {ticker} " + \
                "{timeframe} {start_ms}"
            print(template.format(
                ticker=ticker,
                timeframe=timeframe,
                start_ms=pd.to_datetime(start_ms, unit="ms")))
            url = end_point.format(
                timeframe=timeframe, ticker=ticker,
                start=int(start_ms))
            pd_data = pd.DataFrame(self.get(url), columns=[
                "mts", "open", "close", "high", "low", "volume"])
            pd_data["time"] = pd.to_datetime(pd_data["mts"], unit="ms")
            list_pd_data.append(pd_data)
            start_ms = pd_data["mts"].max() + 1
            if len(pd_data) == 0:
                break

        temp_df = pd.concat(list_pd_data)[[
            "time", "open", "close", "high", "low", "volume"]]
        temp_df["timeframe"] = timeframe
        temp_df["pair_base"] = base_currency.lower()
        temp_df["pair_quote"] = quote_currency.lower()
        return temp_df

    def get_ticker_info(self, tickers: List[str]):
        """Get tickers current information.

        Get current information of ticker in Bitfinex.

        Args:
            tickers List[str]: List of the tickers to retrieve information.
        Return [dict]:
            Returns a dictionary with tickers as keys and dict a as value with
            following keys:
                frr [float]: Flash Return Rate - average of all fixed rate
                    funding over the last hour (funding tickers only).
                bid [float]: Price of last highest bid.
                bid_period [int]: Bid period covered in days (funding tickers
                    only).
                bid_size [float]: Sum of the 25 highest bid sizes.
                ask [float]: Price of last lowest ask.
                ask_period [int]: Ask period covered in days (funding tickers
                    only).
                ask_size [float]: Sum of the 25 lowest ask sizes.
                daily_change [float]: Amount that the last price has changed
                    since yesterday.
                daily_change_relative [float]: Relative price change since
                    yesterday (*100 for percentage change).
                last_price [float]: Price of the last trade.
                volume [float]: Daily volume.
                high [float]: Daily high.
                low [float]: Daily low
                frr_amount_available [float]: The amount of funding that is
                    available at the Flash Return Rate (funding tickers only).
        """
        currency_pairs = pd.Series(tickers).str.lstrip('t')
        index_in_exchange = currency_pairs.isin(self.pairs["currency_pair"])
        not_in_exchange = currency_pairs[~index_in_exchange].tolist()
        if len(not_in_exchange) != 0:
            msg = (
                "Some currency pairs are not available at "
                "BitFinex: {}").format(not_in_exchange)
            print(msg)

        if type(tickers) == list:
            tickers = ",".join(tickers)

        response = self.get(
            end_point="tickers", parameters={"symbols": tickers})
        pd_response = pd.DataFrame(response, columns=[
            "ticker", "bid", "bid_size", "ask", "ask_size",
            "daily_change", "daily_change_relative", "last_price", "volume",
            "high", "low"])
        return pd_response

    def get_wallet(self, calculate_shares: bool = False):
        """Get wallet portfolio.

        Get wallet currency distribution, this is an authenticated end-point.

        Kwargs:
            calculate_shares [bool]: If shares and currency balance should be
                calculated acording to last prince against USD Dollar.
        Return [pd.DataFrame]:
            wallet_type [str]: Type of the wallat EXCHANGE, MARGIN.
            currency [str]: Currency balance.
            balance [float]: Balance over the currency.
            unsettled_interest [float]: Used for margin trading.
            available_balance [float]: Available balance over currency.
            last_change [str]: Last change over the currency.
            trade_detail [dict]: Info of the last trade detail.

            -- Optional if calculate_shares=True
            last_price [str]: Last price of the currency against the
                USD Dollar.
            balance__USD [float]: Value of the currecy converted to dollar
                values.
            share__USD [float]: Share of the currency in USD dollar for
                the all portfolio.
        """
        wallet_portfolio = self.post_authenticated(
            end_point="auth/r/wallets", body={})
        pd_wallet = pd.DataFrame(wallet_portfolio, columns=[
            "wallet_type", "currency", "balance", "unsettled_interest",
            "available_balance", "last_change", "trade_detail"])

        if calculate_shares:
            pd_wallet["ticker"] = ("t" + pd_wallet["currency"] + "USD")
            tickers_info = self.get_ticker_info(
                tickers=pd_wallet["ticker"][
                    pd_wallet["ticker"] != "tUSDUSD"].tolist())
            pd_wallet = pd_wallet.merge(tickers_info, how="left")

            ##########################################################
            # Setting USD exchange rates to 1 for USD dollar vs dollar
            is_usd = pd_wallet["currency"] == "USD"
            pd_wallet.loc[is_usd, "last_price"] = 1

            pd_wallet["balance__USD"] = pd_wallet[
                "balance"]*pd_wallet["last_price"]
            pd_wallet["share__USD"] = pd_wallet[
                "balance__USD"]/pd_wallet["balance__USD"].sum()
            pd_wallet = pd_wallet[[
                "wallet_type", "currency", "balance", "unsettled_interest",
                "available_balance", "last_change", "trade_detail",
                "last_price", "balance__USD", "share__USD"]]
        return pd_wallet

    def post_order(self, order_type: str, symbol: str, amount: float,
                   price: float = None, symbol_type: str = "spot"):
        """
        Post an order on Bitfinix.

        Args:
            order_type [str]: Must be one of the possible bitfinix order types:
                'LIMIT', 'EXCHANGE LIMIT', 'MARKET', 'EXCHANGE MARKET', 'STOP',
                'EXCHANGE STOP', 'STOP LIMIT', 'EXCHANGE STOP LIMIT',
                'TRAILING STOP', 'EXCHANGE TRAILING STOP', 'FOK',
                'EXCHANGE FOK', 'IOC', 'EXCHANGE IOC'
            symbol [str]: Set the currency pair to make the transaction.
            amount [float]: Amount of order to be placed.
        Kwargs:
            price [float]: Price to be used as reference in the transaction.
                Market positions does not user price since it get the best one
                offered in exchange.
            symbol_type [str]: Must be in options:
                'spot'
        """
        order_type_options = [
            'LIMIT', 'EXCHANGE LIMIT', 'MARKET', 'EXCHANGE MARKET', 'STOP',
            'EXCHANGE STOP', 'STOP LIMIT', 'EXCHANGE STOP LIMIT',
            'TRAILING STOP', 'EXCHANGE TRAILING STOP', 'FOK', 'EXCHANGE FOK',
            'IOC', 'EXCHANGE IOC']
        if order_type not in order_type_options:
            raise BitFinexClientException(
                "order_type must be in options: [{}]".format(
                    order_type_options))
        if not (self.pairs["currency_pair"] == symbol).any():
            raise BitFinexClientException(
                "Symbol is not listed in bitfinex check pairs attribute.")

        symbol_type_options = ["spot"]
        if symbol_type not in symbol_type_options:
            raise BitFinexClientException(
                "symbol_type must be in options: [{}]".format(
                    symbol_type_options))

        ticker = None
        if symbol_type == "spot":
            ticker = "t" + symbol

        body = {
            "type": order_type,
            "symbol": ticker,
            "amount": str(amount)}

        if price is not None:
            body["price"] = str(price)

        return self.post_authenticated(
            end_point="auth/w/order/submit", body=body)

    def set_portfolio_share(self, portfolio_shares: pd.DataFrame):
        """Set portfolio share in USD acording to currency value at moment.

        It will always keep at least 5% of the value in USD to keep liquidity
        for buy operations.

        Args:
            portfolio_shares [pd.DataFrame]: Dataframe with two columns:
                currency [str]: Currency abreviation on bitfinex.
                share [float]: Share of the portfolio values that must be on
                    this currency. Share must be in [0, 1] values.
        Return [pd.DataFrame]:
            currency [str]: Currency.
            last_price [float]: Last price for the currency against USD.
            balance__before_order [float]: Balance of the currency in
                portfolio before placing orders.
            value_in_USD__before_order [float]: Correspondent value in USD of
                the currency balance.
            current_share [float]: Current share of the portfolio value in
                USD that is placed in the currency.
            expected_share [float]: Expected share after performing orders.
            target_balance__min_cap [float]: If the expected share place orders
                less then the min_order value for the ticker 't{currency}USD'
                this orders won't be placed and the target balance may vary.
            target_in_USD__min_cap [float]: Target in USD per currency after
                caping the orders that are inferior to the min order value.
            share__min_cap [float]: Expected share between currencies after the
                min_order cap.
            balance__after_order [float]: Balance of each currency after
                orders.
            value_in_USD__after_order [float]: USD values of each currency
                after placing orders.
            share__after_order [float]: Share of the cripto currency after
                placing orders.
            share__diff [float]: Difference between share__after_order and
                share__min_cap.
        Exceptions:

        """
        if not set(portfolio_shares.columns) == {"currency", "share"}:
            raise Exception(
                "portfolio_shares columns must be ['currency', 'share']")

        portfolio_shares["currency"] = portfolio_shares["currency"].str.upper()
        not_listed_currency = portfolio_shares["currency"][
            ~portfolio_shares["currency"].isin(self.currencies["symbol"])]
        if len(not_listed_currency) != 0:
            msg = ("Some currency for set share are not "
                   "listed on bitfinex: \n{}").format(not_listed_currency)
            raise BitFinexClientException(msg)

        #######################################################################
        # Keep at least 5% on USD to guarantee trade possibility, always have #
        # some money to make market orders                                    #
        usd_share = portfolio_shares.loc[
            portfolio_shares["currency"] == "USD", "share"]
        if (usd_share < 0.05).bool():
            msg = (
                "## USD share was bellow 5% and limit liquidity threshold. "
                "Shares will be ajusted to keep at least 5% on USD.")
            print(msg)
            index_not_usd = portfolio_shares["currency"] != "USD"
            share_not_usd = portfolio_shares.loc[index_not_usd, "share"].sum()
            portfolio_shares.loc[
                index_not_usd, "share"] = portfolio_shares.loc[
                    index_not_usd, "share"] * (0.95/share_not_usd)
            portfolio_shares.loc[~index_not_usd, "share"] = 0.05

        if not np.isclose(portfolio_shares['share'].sum(), 1):
            msg = "Share does not sum 100%."
            raise BitFinexClientException(msg)

        wallet_values = self.get_wallet()[["currency", "balance"]]
        is_low_value = wallet_values["balance"] < 0.00001
        wallet_values = wallet_values[~is_low_value]
        wallet_values = wallet_values.merge(portfolio_shares, how="outer")
        wallet_values.fillna(value=0, inplace=True)

        ##########################################
        # Getting max and min transaction values #
        wallet_values["currency_pair"] = wallet_values["currency"] + "USD"
        wallet_values = wallet_values.merge(
            self.pairs, how="left", validate="one_to_one")
        not_in_pair = set(wallet_values.loc[
            wallet_values["min_order"].isna(), "currency_pair"])
        if not_in_pair != set(["USDUSD"]):
            not_in_pair = not_in_pair - set(["USDUSD"])
            msg = ("Some pair are not at exchage and can not "
                   "be traded: {}").format(not_in_pair)
            raise BitFinexClientException(msg)

        wallet_values["max_order"].fillna(value=math.inf, inplace=True)
        wallet_values["min_order"].fillna(value=0, inplace=True)
        ##########################################

        #################################################
        # Getting ticker last values to ajust positions #
        wallet_values["ticker"] = "t" + wallet_values["currency"] + "USD"
        ticker_list = list(wallet_values.loc[
            wallet_values["ticker"] != "tUSDUSD", "ticker"])
        tickers_info = self.get_ticker_info(tickers=ticker_list)
        wallet_values = wallet_values.merge(
            tickers_info[["ticker", "last_price", "volume"]], how="left",
            validate="one_to_one")

        # Checking if only USD has not last price
        set_missing_last_price = set(wallet_values.loc[
            wallet_values["last_price"].isna(), "ticker"])
        if set_missing_last_price != set(["tUSDUSD"]):
            msg = "There are some missing prices at tickers: {}".format(
                set_missing_last_price)
            raise BitFinexClientException(msg)

        wallet_values["last_price"].fillna(value=1, inplace=True)
        wallet_values["volume"].fillna(value=math.inf, inplace=True)
        wallet_values["value_in_USD"] = wallet_values[
            "balance"]*wallet_values["last_price"]
        total_value = wallet_values["value_in_USD"].sum()
        wallet_values[
            "target_in_USD"] = wallet_values["share"]*total_value
        wallet_values["target_balance"] = wallet_values[
            "target_in_USD"]/wallet_values["last_price"]
        wallet_values["balance_diff"] = wallet_values[
            "target_balance"] - wallet_values["balance"]

        #######################################################################
        # If transations are bellow min_order, do not do them...
        index_not_gt_min_order = wallet_values[
            "balance_diff"].abs() < wallet_values["min_order"]
        wallet_values["balance_diff__min_cap"] = wallet_values["balance_diff"]
        wallet_values.loc[index_not_gt_min_order, "balance_diff__min_cap"] = 0

        #######################################################################
        # Performing the transations acording to values and executing sell ones
        # first to guarantee transaction funds.
        # USDUSD transaction can't be performed and are the result of sell and
        # buy ones
        index_not_zero = wallet_values["balance_diff__min_cap"] != 0
        index_not_USD = wallet_values["currency"] != "USD"
        to_order = wallet_values.loc[
            index_not_zero & index_not_USD,
            ["currency", "currency_pair", "balance_diff__min_cap"]]
        to_order.sort_values("balance_diff__min_cap", inplace=True)

        for index, row in to_order.iterrows():
            # Make EXCHANGE MARKET order so there is no need to wait for
            # transaction fulfillment.
            self.post_order(
                order_type="EXCHANGE MARKET",
                symbol=row["currency_pair"],
                amount=row["balance_diff__min_cap"])

        #######################################################################
        # Ajusting the expected share acording to limitations on min_order
        # value
        wallet_values["target_balance__min_cap"] = wallet_values[
            "balance"] + wallet_values["balance_diff__min_cap"]
        wallet_values["target_in_USD__min_cap"] = wallet_values[
            "target_balance__min_cap"] * wallet_values[
                "last_price"]
        wallet_values["share__min_cap"] = wallet_values[
            "target_in_USD__min_cap"] / wallet_values[
                "target_in_USD__min_cap"].sum()

        after_order_wallet = self.get_wallet()[["currency", "balance"]]
        after_order_wallet.rename(columns={
            "balance": "balance__after_order"}, inplace=True)

        after_ajusts = wallet_values.merge(
            after_order_wallet, validate="one_to_one")
        after_ajusts.rename(columns={
            "balance": "balance__before_order",
            "value_in_USD": "value_in_USD__before_order",
            "share": "expected_share"}, inplace=True)
        after_ajusts = after_ajusts[[
            "currency", "last_price", "balance__before_order",
            "value_in_USD__before_order", "expected_share",
            "target_balance__min_cap", "target_in_USD__min_cap",
            "share__min_cap", "balance__after_order"]].copy()
        after_ajusts["value_in_USD__after_order"] = after_ajusts[
            "last_price"] * after_ajusts["balance__after_order"]
        after_ajusts["share__after_order"] = after_ajusts[
            "value_in_USD__after_order"] / after_ajusts[
            "value_in_USD__after_order"].sum()
        after_ajusts["current_share"] = after_ajusts[
            "value_in_USD__before_order"] / after_ajusts[
            "value_in_USD__before_order"].sum()

        after_ajusts = after_ajusts[[
            'currency', 'last_price', 'balance__before_order',
            'value_in_USD__before_order', 'current_share',
            'expected_share', 'target_balance__min_cap',
            'target_in_USD__min_cap', 'share__min_cap',
            'balance__after_order', 'value_in_USD__after_order',
            'share__after_order']]

        after_ajusts["share__diff"] = after_ajusts[
            "share__min_cap"] - after_ajusts["share__after_order"]
        max_diff_shares = after_ajusts["share__diff"].abs().max()

        ######################################################
        # Checking if the values are distant of the expected #
        if 0.05 < max_diff_shares:
            msg = (
                "Something went wrong when ajusting portfolio share, "
                "difference between expected share and after order share is "
                "greater than 5%. Max difference={:.2%}\n").format(
                    max_diff_shares)
            msg = msg + after_ajusts.to_string()
            raise BitFinexClientException(msg)
        return after_ajusts

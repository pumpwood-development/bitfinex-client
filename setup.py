#!/usr/bin/python
# -*- coding: utf-8 -*-
"""module setup."""

import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='pumpwood_bitfinex_client',
    version='0.7',
    packages=find_packages(),
    include_package_data=True,
    license='',  # example license
    description='Bitfinex client developed by Murabei Data Science',
    long_description=README,
    url='',
    author='André Andrade Baceti',
    author_email='a.baceti@murabei.com',
    classifiers=[
    ],
    install_requires=[

    ],
    dependency_links=[]
)
